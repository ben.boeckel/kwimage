"""
Generated on 2022-08-23 via ``johnnydep kwimage[all]``
"""

kwimage[all]                                   The Kitware Image Module
├── Pillow                                     Python Imaging Library (Fork)
├── PyTurboJPEG                                A Python wrapper of libjpeg-turbo for decoding and encoding JPEG image.
│   └── numpy                                  NumPy is the fundamental package for array computing with Python.
├── coverage                                   Code coverage measurement for Python
├── distinctipy                                A lightweight package for generating visually distinct colours.
│   └── numpy                                  NumPy is the fundamental package for array computing with Python.
├── itk-io                                     ITK is an open-source toolkit for multidimensional image analysis
│   └── itk-core==5.3rc4.post2                 ITK is an open-source toolkit for multidimensional image analysis
│       └── numpy                              NumPy is the fundamental package for array computing with Python.
├── kwarray
│   ├── numpy                                  NumPy is the fundamental package for array computing with Python.
│   ├── scipy                                  SciPy: Scientific Library for Python
│   │   └── numpy<1.25.0,>=1.18.5              NumPy is the fundamental package for array computing with Python.
│   ├── six                                    Python 2 and 3 compatibility utilities
│   └── ubelt                                  A Python utility belt containing simple tools, a stdlib like feel, and extra batteries.
├── kwimage-ext                                The kwimage_ext Module
│   ├── numpy                                  NumPy is the fundamental package for array computing with Python.
│   └── ubelt                                  A Python utility belt containing simple tools, a stdlib like feel, and extra batteries.
├── matplotlib                                 Python plotting package
│   ├── cycler>=0.10                           Composable style cycles
│   ├── fonttools>=4.22.0                      Tools to manipulate font files
│   ├── kiwisolver>=1.0.1                      A fast implementation of the Cassowary constraint solver
│   ├── numpy>=1.17                            NumPy is the fundamental package for array computing with Python.
│   ├── packaging>=20.0                        Core utilities for Python packages
│   │   └── pyparsing!=3.0.5,>=2.0.2           pyparsing module - Classes and methods to define and execute parsing grammars
│   ├── pillow>=6.2.0                          Python Imaging Library (Fork)
│   ├── pyparsing>=2.2.1                       pyparsing module - Classes and methods to define and execute parsing grammars
│   └── python-dateutil>=2.7                   Extensions to the standard Python datetime module
│       └── six>=1.5                           Python 2 and 3 compatibility utilities
├── numpy                                      NumPy is the fundamental package for array computing with Python.
├── parse                                      parse() is the opposite of format()
├── pytest                                     pytest: simple powerful testing with Python
│   ├── attrs>=19.2.0                          Classes Without Boilerplate
│   ├── iniconfig                              iniconfig: brain-dead simple config-ini parsing
│   ├── packaging                              Core utilities for Python packages
│   │   └── pyparsing!=3.0.5,>=2.0.2           pyparsing module - Classes and methods to define and execute parsing grammars
│   ├── pluggy<2.0,>=0.12                      plugin and hook calling mechanisms for python
│   ├── py>=1.8.2                              library with cross-python path, ini-parsing, io, code, log facilities
│   └── tomli>=1.0.0                           A lil' TOML parser
├── pytest-cov                                 Pytest plugin for measuring coverage.
│   ├── coverage[toml]>=5.2.1                  Code coverage measurement for Python
│   │   └── tomli                              A lil' TOML parser
│   └── pytest>=4.6                            pytest: simple powerful testing with Python
│       ├── attrs>=19.2.0                      Classes Without Boilerplate
│       ├── iniconfig                          iniconfig: brain-dead simple config-ini parsing
│       ├── packaging                          Core utilities for Python packages
│       │   └── pyparsing!=3.0.5,>=2.0.2       pyparsing module - Classes and methods to define and execute parsing grammars
│       ├── pluggy<2.0,>=0.12                  plugin and hook calling mechanisms for python
│       ├── py>=1.8.2                          library with cross-python path, ini-parsing, io, code, log facilities
│       └── tomli>=1.0.0                       A lil' TOML parser
├── scikit-image                               Image processing in Python
│   ├── PyWavelets>=1.1.1                      PyWavelets, wavelet transform module
│   │   └── numpy>=1.17.3                      NumPy is the fundamental package for array computing with Python.
│   ├── imageio>=2.4.1                         Library for reading and writing a wide range of image, video, scientific, and volumetric data formats.
│   │   ├── numpy                              NumPy is the fundamental package for array computing with Python.
│   │   └── pillow>=8.3.2                      Python Imaging Library (Fork)
│   ├── networkx>=2.2                          Python package for creating and manipulating graphs and networks
│   ├── numpy>=1.17.0                          NumPy is the fundamental package for array computing with Python.
│   ├── packaging>=20.0                        Core utilities for Python packages
│   │   └── pyparsing!=3.0.5,>=2.0.2           pyparsing module - Classes and methods to define and execute parsing grammars
│   ├── pillow!=7.1.0,!=7.1.1,!=8.3.0,>=6.1.0  Python Imaging Library (Fork)
│   ├── scipy>=1.4.1                           SciPy: Scientific Library for Python
│   │   └── numpy<1.25.0,>=1.18.5              NumPy is the fundamental package for array computing with Python.
│   └── tifffile>=2019.7.26                    Read and write TIFF files
│       └── numpy>=1.19.2                      NumPy is the fundamental package for array computing with Python.
├── scipy                                      SciPy: Scientific Library for Python
│   └── numpy<1.25.0,>=1.18.5                  NumPy is the fundamental package for array computing with Python.
├── shapely                                    Geometric objects, predicates, and operations
├── timerit                                    A powerful multiline alternative to timeit
├── torch                                      Tensors and Dynamic neural networks in Python with strong GPU acceleration
│   └── typing-extensions                      Backported and Experimental Type Hints for Python 3.7+
├── ubelt                                      A Python utility belt containing simple tools, a stdlib like feel, and extra batteries.
└── xdoctest                                   A rewrite of the builtin doctest module
    └── six                                    Python 2 and 3 compatibility utilities


colormath          Color math and conversion library.
├── networkx>=2.0  Python package for creating and manipulating graphs and networks
└── numpy          NumPy is the fundamental package for array computing with Python.
