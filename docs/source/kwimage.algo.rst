kwimage.algo package
====================

Submodules
----------

.. toctree::
   :maxdepth: 4

   kwimage.algo.algo_nms

Module contents
---------------

.. automodule:: kwimage.algo
   :members:
   :undoc-members:
   :show-inheritance:
