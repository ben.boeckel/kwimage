kwimage package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   kwimage.algo
   kwimage.structs

Submodules
----------

.. toctree::
   :maxdepth: 4

   kwimage.im_alphablend
   kwimage.im_color
   kwimage.im_core
   kwimage.im_cv2
   kwimage.im_demodata
   kwimage.im_draw
   kwimage.im_filter
   kwimage.im_io
   kwimage.im_runlen
   kwimage.im_stack
   kwimage.transform
   kwimage.util_warp

Module contents
---------------

.. automodule:: kwimage
   :members:
   :undoc-members:
   :show-inheritance:
